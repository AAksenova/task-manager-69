<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
	<head>
	    <style>
	        #header{
            	background-color: white;
            	color: black;
            	width: 100%;
                height: 60px;
                border: 2px solid;
            }

            #header a {
            	color: black;
            	text-decoration: none;
            	margin: 20px;
            	display: inline-block;
            }

            table {
            	margin-left: 20px;
            	margin-top: 25px;
            	border: 2px solid;
            	width: 1500px;
            }

            th {
            	border: 1px solid grey;
            }

            td {
            	border: 1px solid grey;
            }

            .footer {
            	margin-left: 50%;
            	margin-top: 800px;
            }
	    </style>
		<title>Task Manager</title>
	</head>
	<body>
		<div id="header">
		    <sec:authorize access="isAuthenticated()">
			<a href="/">MAIN</a> <a href="/projects">PROJECTS</a> <a href="/tasks">TASKS</a>User: <sec:authentication property="name"/><a href="/logout">Logout</a>
			</sec:authorize>
			<sec:authorize access="!isAuthenticated()">
			    <a href="/login">Login</a>
			</sec:authorize>
		</div>
