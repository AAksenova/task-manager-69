package ru.t1.aksenova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.aksenova.tm.entity.dto.TaskDTO;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)

public interface TaskDTORepository extends JpaRepository<TaskDTO, String> {

    @Transactional
    void deleteByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    List<TaskDTO> findAllByUserId(@NotNull String userId);

    @NotNull
    Optional<TaskDTO> findByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

    @Transactional
    void deleteAllByUserId(@NotNull String userId);

}
