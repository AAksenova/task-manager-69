package ru.t1.aksenova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.aksenova.tm.entity.model.User;

@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public interface UserRepository extends JpaRepository<User, String> {

    User findByLogin(@NotNull String login);

    Boolean existsByLogin(@NotNull final String login);

}
