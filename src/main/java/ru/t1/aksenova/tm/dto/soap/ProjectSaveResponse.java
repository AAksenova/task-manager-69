package ru.t1.aksenova.tm.dto.soap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectSaveResponse")
public class ProjectSaveResponse {

    @NotNull
    @XmlElement(name = "project")
    private ProjectDTO project;

}
