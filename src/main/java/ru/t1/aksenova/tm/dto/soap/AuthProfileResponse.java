package ru.t1.aksenova.tm.dto.soap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.entity.model.User;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlType(name = "", propOrder = {
        "user"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "authProfileResponse")
public class AuthProfileResponse {

    @Nullable
    @XmlElement(name = "user")
    protected User user;

}
