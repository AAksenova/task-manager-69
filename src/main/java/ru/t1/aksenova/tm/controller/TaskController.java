package ru.t1.aksenova.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.aksenova.tm.api.service.dto.IProjectDTOService;
import ru.t1.aksenova.tm.api.service.dto.ITaskDTOService;
import ru.t1.aksenova.tm.entity.dto.CustomUser;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;
import ru.t1.aksenova.tm.entity.dto.TaskDTO;
import ru.t1.aksenova.tm.enumerated.Status;

import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private ITaskDTOService taskService;

    @Autowired
    private IProjectDTOService projectService;


    @GetMapping("task/create")
    @PreAuthorize("isAuthenticated()")
    public String create(@AuthenticationPrincipal @NotNull final CustomUser user) {
        taskService.add(new TaskDTO(user.getUserId(), "New Task " + System.currentTimeMillis(), Status.NOT_STARTED));
        return "redirect:/tasks";
    }

    @GetMapping("task/delete/{id}")
    @PreAuthorize("isAuthenticated()")
    public String delete(@AuthenticationPrincipal @NotNull final CustomUser user, @PathVariable("id") String id) {
        taskService.removeOneById(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @PostMapping("task/edit/{id}")
    @PreAuthorize("isAuthenticated()")
    public String edit(@AuthenticationPrincipal @NotNull final CustomUser user,
                       @ModelAttribute("task") TaskDTO task,
                       BindingResult result
    ) {
        if (task.getProjectId() == null) task.setProjectId(null);
        task.setUserId(user.getUserId());
        taskService.update(task);
        return "redirect:/tasks";
    }

    @GetMapping("task/edit/{id}")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView edit(@AuthenticationPrincipal @NotNull final CustomUser user, @PathVariable("id") String id) {
        final TaskDTO task = taskService.findOneById(user.getUserId(), id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", getProjects(user));
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    public Status[] getStatuses() {
        return Status.values();
    }

    private Collection<ProjectDTO> getProjects(@AuthenticationPrincipal @NotNull final CustomUser user) {
        return projectService.findAll(user.getUserId());
    }

}
