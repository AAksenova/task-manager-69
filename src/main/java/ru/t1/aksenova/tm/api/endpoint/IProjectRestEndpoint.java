package ru.t1.aksenova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;

import java.util.List;

@RequestMapping("/api/projects")
public interface IProjectRestEndpoint {

    @NotNull
    @PostMapping("/add")
    ProjectDTO add(@RequestBody @NotNull ProjectDTO project);

    @GetMapping("/findAll")
    List<ProjectDTO> findAll();

    @NotNull
    @PostMapping("/save")
    void save(@RequestBody @NotNull ProjectDTO project);

    @Nullable
    @GetMapping("/findById/{id}")
    ProjectDTO findById(@PathVariable("id") @NotNull String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") @NotNull String id);

    @GetMapping("/count")
    long count();

    @PostMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") @NotNull String id);

    @PostMapping("/delete")
    void delete(@RequestBody @NotNull ProjectDTO project);

    void clear();
}
