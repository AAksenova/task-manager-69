package ru.t1.aksenova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.entity.dto.CustomUser;
import ru.t1.aksenova.tm.entity.model.Role;
import ru.t1.aksenova.tm.entity.model.User;
import ru.t1.aksenova.tm.enumerated.RoleType;
import ru.t1.aksenova.tm.exception.user.AccessDeniedException;
import ru.t1.aksenova.tm.repository.model.UserRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws AccessDeniedException {
        final User user = findByLogin(username);
        if (user == null) throw new UsernameNotFoundException("User not found.");

        final List<Role> userRoles = user.getRoles();
        final List<String> roles = new ArrayList<>();
        for (Role role : userRoles) roles.add(role.toString());

        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[]{}))
                .build())
                .withUserId(user.getId());
    }

    @PostConstruct
    private void init() {
        initUser("admin", "admin", RoleType.ADMINISTRATOR);
        initUser("test", "test", RoleType.USER);
    }

    private User findByLogin(String username) {
        return userRepository.findByLogin(username);
    }

    private void initUser(@NotNull final String login,
                          @NotNull final String password,
                          @NotNull final RoleType roleType
    ) {
        final User user = userRepository.findByLogin(login);
        if (user != null) return;
        createUser(login, password, roleType);
    }

    @Transactional
    public void createUser(@NotNull final String login,
                           @NotNull final String password,
                           @NotNull final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        final String passwordHash = passwordEncoder.encode(password);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        final Role role = new Role();
        role.setUser(user);
        role.setRoleType(roleType);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
    }
}