package ru.t1.aksenova.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.api.service.dto.IProjectDTOService;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aksenova.tm.exception.field.*;
import ru.t1.aksenova.tm.repository.dto.ProjectDTORepository;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDTOService implements IProjectDTOService {

    @Nullable
    @Autowired
    private ProjectDTORepository repository;

    @NotNull
    @Override
    public ProjectDTORepository getRepository() {
        return repository;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO add(@NotNull final ProjectDTO project) {
        getRepository().save(project);
        return project;
    }

    @Override
    @Transactional
    public void update(@NotNull ProjectDTO project) {
        getRepository().save(project);
    }

    @Override
    @Transactional
    public void remove(@NotNull ProjectDTO project) {
        getRepository().delete(project);
    }

    @NotNull
    @Override
    public ProjectDTO create(@Nullable final ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        add(project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @Nullable String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        add(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<ProjectDTO> set(@NotNull Collection<ProjectDTO> projects) {
        if (projects.isEmpty()) return Collections.emptyList();
        projects.forEach(getRepository()::save);
        return projects;
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().findAllByUserId(userId);
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(
            @Nullable String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Optional<ProjectDTO> project = getRepository().findByUserIdAndId(userId, id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        return project.orElse(null);
    }

    @Override
    @Transactional
    public void clear() {
        getRepository().deleteAll();
    }

    @Override
    @Transactional
    public void removeAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        getRepository().deleteAllByUserId(userId);
    }

    @Nullable
    @Override
    public ProjectDTO removeOne(
            @Nullable String userId,
            @Nullable final ProjectDTO project
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ProjectNotFoundException();
        if (!project.getUserId().equals(userId)) throw new ProjectNotFoundException();
        remove(project);
        return project;
    }

    @Nullable
    @Override
    public ProjectDTO removeOneById(
            @Nullable String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        remove(project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO updateById(
            @Nullable String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO changeProjectStatusById(
            @Nullable String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
        return project;
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        return getRepository().findByUserIdAndId(userId, id).orElse(null) != null;
    }

    @Override
    public long getSize(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().countByUserId(userId);
    }

}
