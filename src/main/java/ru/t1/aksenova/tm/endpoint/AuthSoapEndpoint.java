package ru.t1.aksenova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.aksenova.tm.api.service.dto.IUserService;
import ru.t1.aksenova.tm.dto.soap.*;
import ru.t1.aksenova.tm.entity.dto.Result;

@Endpoint
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class AuthSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws/auth";

    @NotNull
    public final static String PORT_TYPE_NAME = "AuthSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://aksenova.t1.ru/tm/dto/soap";

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "authLoginRequest", namespace = NAMESPACE)
    public AuthLoginResponse login(@NotNull @RequestPayload final AuthLoginRequest request) {
        try {
            @NotNull final String username = request.getLogin();
            @NotNull final String password = request.getPassword();
            @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            @NotNull final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new AuthLoginResponse(new Result(authentication.isAuthenticated()));
        } catch (@NotNull final Exception e) {
            return new AuthLoginResponse(new Result(e));
        }
    }

    @NotNull
    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "authProfileRequest", namespace = NAMESPACE)
    public AuthProfileResponse profile(@NotNull @RequestPayload final AuthProfileRequest request) {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = securityContext.getAuthentication();
        @NotNull final String username = authentication.getName();
        return new AuthProfileResponse(userService.findByLogin(username));
    }

    @NotNull
    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "authLogoutRequest", namespace = NAMESPACE)
    public AuthLogoutResponse logout(@NotNull @RequestPayload final AuthLogoutRequest request) {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new AuthLogoutResponse(new Result());
    }

}
