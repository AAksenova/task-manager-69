package ru.t1.aksenova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.client.ProjectRestEndpointClient;
import ru.t1.aksenova.tm.entity.dto.ProjectDTO;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.marker.IntegrationCategory;

import java.util.Collections;
import java.util.List;

import static ru.t1.aksenova.tm.constant.ProjectTestData.*;

@Category(IntegrationCategory.class)
public class ProjectRestEndpointTest {

    @NotNull
    final static ProjectRestEndpointClient client = ProjectRestEndpointClient.client();

    @NotNull
    private static final ProjectDTO projectOne = new ProjectDTO(ONE_PROJECT_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @NotNull
    private static final ProjectDTO projectTwo = new ProjectDTO(TWO_PROJECT_NAME, Status.valueOf(STATUS_NOT_STARTED));

    @NotNull
    private static final ProjectDTO projectThree = new ProjectDTO(THREE_PROJECT_NAME, Status.valueOf(STATUS_NOT_STARTED));


    @BeforeClass
    public static void setUp() {
        client.add(projectOne);
        client.add(projectTwo);
    }

    @AfterClass
    public static void tearDown() {
        client.delete(projectOne);
        client.delete(projectTwo);
        client.delete(projectThree);
    }

    @Test
    public void add() {
        @NotNull final String projectName = projectThree.getName();
        @Nullable final ProjectDTO project = client.add(projectThree);
        Assert.assertNotNull(project);
        Assert.assertEquals(projectName, project.getName());
    }

    @Test
    public void save() {
        @Nullable ProjectDTO project = client.findById(projectTwo.getId());
        Assert.assertNotNull(project);
        project.setDescription(TWO_PROJECT_DESCRIPTION);
        project.setStatus(Status.valueOf(STATUS_IN_PROGRESS));
        client.save(project);
        @Nullable ProjectDTO project2 = client.findById(projectTwo.getId());
        Assert.assertNotNull(project2);
        Assert.assertEquals(project.getDescription(), project2.getDescription());
        Assert.assertEquals(project.getStatus(), project2.getStatus());
    }

    @Test
    public void findAll() {
        final long projectSize = client.count();
        @Nullable final List<ProjectDTO> projects = client.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectSize, projects.size());
        projects.forEach(project -> Assert.assertNotNull(project.getId()));
    }

    @Test
    public void findById() {
        @NotNull final String projectId = projectOne.getId();
        @Nullable final ProjectDTO project = client.findById(projectId);
        Assert.assertNotNull(project);
        Assert.assertEquals(projectId, project.getId());
    }

    @Test
    public void delete() {
        Assert.assertNotNull(client.findById(projectOne.getId()));
        client.delete(projectOne);
        Assert.assertThrows(Exception.class, () -> client.findById(projectOne.getId()));
        client.add(projectOne);
    }

    @Test
    public void deleteById() {
        @NotNull final String projectId = projectOne.getId();
        Assert.assertNotNull(client.findById(projectId));
        client.deleteById(projectId);
        Assert.assertThrows(Exception.class, () -> client.findById(projectId));
        client.add(projectOne);
    }

    @Test
    public void count() {
        final long projectSize = client.count();
        @Nullable final List<ProjectDTO> projects = client.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectSize, projects.size());
    }

    @Test
    public void existsById() {
        Assert.assertTrue(client.existsById(projectTwo.getId()));
        @NotNull final ProjectDTO projectFour = new ProjectDTO(FOUR_PROJECT_NAME, Status.valueOf(STATUS_COMPLETE));
        Assert.assertThrows(Exception.class, () -> client.existsById(projectFour.getId()));
    }

    @Test
    public void clear() {
        @Nullable final List<ProjectDTO> projects = client.findAll();
        Assert.assertNotNull(projects);
        client.clear();
        @Nullable final List<ProjectDTO> projects2 = client.findAll();
        Assert.assertNotNull(projects2);
        Assert.assertEquals(Collections.emptyList(), projects2);
        Assert.assertEquals(0, projects2.size());
        for (@NotNull ProjectDTO project : projects) {
            client.add(project);
        }
    }

}
